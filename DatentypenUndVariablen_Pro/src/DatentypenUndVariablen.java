/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Kvast, Maxim >>
  */

public class DatentypenUndVariablen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
/*  *********************************************************
	    
        Zuerst werden die Variablen mit den Werten festgelegt!
   
   *********************************************************** */
   // Im Internet gefunden ?
   // Die Anzahl der Planeten in unserem Sonnesystem                    
   int anzahlPlaneten = 9 ;
   
   // Anzahl der Sterne in unserer Milchstraße
   long anzahlSterne = 1000000000L ;
   
   long anzahlSterne2 = 4000000000L ;
   
   // Wie viele Einwohner hat Berlin?
   int bewohnerBerlin = 3645000 ;
   
   // Wie alt bist du?  Wie viele Tage sind das?
   
   short alterTage = 7187 ;
   
   // Wie viel wiegt das schwerste Tier der Welt?
   // Schreiben Sie das Gewicht in Kilogramm auf!
   int gewichtKilogramm = 150000 ;
   
   // Schreiben Sie auf, wie viele km² das größte Land der Erde hat?
   int flaecheGroessteLand = 17130000 ;
   
   // Wie groß ist das kleinste Land der Erde?
   
   double flaecheKleinsteLand = 0.44 ;
   
   
   
   
   /*  *********************************************************
   
        Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
   
   *********************************************************** */
   
   System.out.println("Anzhahl der Planeten: " + anzahlPlaneten + "\n");
   
   System.out.println("Anzahl der Sterne: " + anzahlSterne + " bis " + anzahlSterne2 + "\n");
   
   System.out.println("Bewohner in Berlin: " + bewohnerBerlin +"\n");
   
   System.out.println("Das Alter in Tagen: " + alterTage + "\n");
   
   System.out.println("Gewicht des schwersten Tieres: " + "Blauwal mit " + gewichtKilogramm + "KG" + "\n") ;
   
   System.out.println("Fläche des größten Landes: " + "Russland mit " + flaecheGroessteLand + "km²" + "\n") ;
   
   System.out.println("Fläche des kleinsten Landes: " + "Vatikan mit " + flaecheKleinsteLand + "km²" + "\n") ;
   
   System.out.println(" *******  Ende des Programms  ******* ");
   
	}

}
