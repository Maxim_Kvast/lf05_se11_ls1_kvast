
public class Konsolenausgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Aufgabenformatierung 1 \n\n");
		
		
		System.out.println("Aufgabe 1: \n");
		
		System.out.println("Ich habe 3 Beine."
				+ " Ich habe aber auch 3 Arme. \n");
		
		System.out.println("Ich habe 3 Beine.");
		System.out.println("Ich habe aber auch 3 Arme. \n");
		
		
//		print() führt keinen Zeilenvorschub durch
//		println() führt jedoch schon
		
		System.out.println("Aufgabe 2: \n");
	
		System.out.printf( "%9s\n%10s\n%11s\n%12s\n%13s\n%14s\n%15s\n%10s\n%12s\n", "*", "***", "*****" , "*******" , " *********" , " ***********", "*************" , "***" , "*** \n" );
		
		
		System.out.println("Aufgabe 3: \n");
		
		System.out.printf("%.2f\n%.2f\n%.2f\n%.2f\n%.2f\n" , 22.4234234 , 111.2222 , 4.0 , 1000000.551 , 97.34 );
		
		
	}

}
